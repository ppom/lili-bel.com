/** @type {import('tailwindcss').Config} */
const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
	content: ["./templates/*.html", "./templates/**/*.html"],
	theme: {
		extend: {
			fontFamily: {
				'sans': ['Aller', ...defaultTheme.fontFamily.sans],
			},
			screens: {
				'smfooter': '558px',
			},
		},
	},
	plugins: [
		require('@tailwindcss/typography'),
	],
}
