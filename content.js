const CATEGORIES_URL = "https://edit.ppom.fr/items/lili_categories?fields[]=*&filter[status][_eq]=published&limit=-1";
const OEUVRES_URL = "https://edit.ppom.fr/items/lili_oeuvres?fields[]=*&fields[]=categorie.nom&fields[]=materiaux.lili_materiaux_id.Nom&fields[]=techniques.lili_techniques_id.nom&fields[]=images.directus_files_id&filter[status][_eq]=published&deep[images][_sort]=sort&deep[materiaux][_sort]=sort&deep[techniques][_sort]=sort&limit=-1";
const ARTICLES_URL = "https://edit.ppom.fr/items/lili_articles?fields[]=*&fields[]=images.directus_files_id&deep[images][_sort]=sort";

// https://byby.dev/js-slugify-string
function slugify(str) {
	return String(str)
		.normalize('NFKD')
		.replace(/[\u0300-\u036f]/g, '')
		.trim()
		.toLowerCase()
		.replace(/[^a-z0-9 -]/g, '')
		.replace(/\s+/g, '-')
		.replace(/-+/g, '-');
}

async function articles() {
	const response = await fetch(ARTICLES_URL);
	const articles = await response.json()

	return articles.data.flatMap(article => ([
		{
			type: "text",
			path: `${slugify(article.titre)}.md`,
			header: {
				title: article.titre || "",
				template: "article.html",
				weight: 0,
				in_search_index: true,
			},
			header_extra: {
				image: article.image || "",
				texte: article.texte || "",
				nb_images: article.images.length || 0,
				...Object.fromEntries(article.images.map(({ directus_files_id: image }, n) => ([`img${n}`, image]))),
			}
		},
		...article.images.map(({ directus_files_id: image }) => ({
			type: "download",
			path: `../static/img/big/${image}.webp`,
			url: `https://edit.ppom.fr/assets/${image}?download&key=big`,
		})),
	]));
}

function dimensions(oeuvre) {
	const { hauteur, largeur, profondeur } = oeuvre;
	if (!hauteur || !largeur) {
		return "";
	}
	let dim = `${hauteur || ""} x ${largeur || ""}`;
	if (profondeur) {
		dim += ` x ${oeuvre.profondeur}`;
	}
	dim += " cm";
	return dim;
}

async function oeuvres() {
	const response = await fetch(OEUVRES_URL);
	const oeuvres = await response.json()

	return oeuvres.data.flatMap(oeuvre => ([
		{
			type: "text",
			path: `${slugify(oeuvre.categorie.nom)}/${slugify(oeuvre.titre)}.md`,
			header: {
				title: oeuvre.titre || "",
				weight: oeuvre.sort || 0,
				in_search_index: true,
			},
			header_extra: {
				image: oeuvre.image_principale || "",
				nb_images: oeuvre.images.length || 0,
				...Object.fromEntries(oeuvre.images.map(({ directus_files_id: image }, n) => ([`img${n}`, image]))),
				description_top: oeuvre.description_top || "",
				description_bottom: oeuvre.description_bottom || "",
				bergere: oeuvre.bergere || false,
				annee: oeuvre.annee || "",
				lieu: oeuvre.lieu || "",
				dimensions: dimensions(oeuvre),
				techniques: oeuvre.techniques.map(tech => tech.lili_techniques_id.nom).join(', '),
				materiaux: oeuvre.materiaux.map(materiau => materiau.lili_materiaux_id.Nom).join(', '),
			}
		},
		...(oeuvre.image_principale ? [{
			type: "download",
			path: `../static/img/big/${oeuvre.image_principale}.webp`,
			url: `https://edit.ppom.fr/assets/${oeuvre.image_principale}?download&key=big`,
		},
		{
			type: "download",
			path: `../static/img/square/${oeuvre.image_principale}.webp`,
			url: `https://edit.ppom.fr/assets/${oeuvre.image_principale}?download&key=square`,
		}] : []),
		...oeuvre.images.map(({ directus_files_id: image }) => ({
			type: "download",
			path: `../static/img/big/${image}.webp`,
			url: `https://edit.ppom.fr/assets/${image}?download&key=big`,
		})),
	]));
}

async function categories() {
	const response = await fetch(CATEGORIES_URL);
	const categories = await response.json()

	return categories.data.flatMap(categorie => ([
		{
			type: "text",
			path: `${slugify(categorie.nom)}/_index.md`,
			header: {
				title: categorie.nom || "",
				weight: categorie.sort || 0,
				sort_by: "weight",
				in_search_index: true,
			},
			header_extra: {
				image: categorie.image || "",
			},
		},
		...(categorie.image ? [{
			type: "download",
			path: `../static/img/square/${categorie.image}.webp`,
			url: `https://edit.ppom.fr/assets/${categorie.image}?download&key=square`,
		}] : [])
	]));
}

async function main() {
	console.log(JSON.stringify({
		paths: [
			{
				type: "text",
				path: "_index.md",
				header: {
					sort_by: "weight"
				}
			},
			...await oeuvres(),
			...await categories(),
			...await articles(),
		]
	}));
}

main();
